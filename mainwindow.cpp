#include "vtkAutoInit.h"
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <vtkImageData.h>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent,
                       std::unique_ptr<IImageViewer> p_imageViewer,
                       std::unique_ptr<IDicomImageReader> p_reader,
                       vtkSmartPointer<vtkImageViewer2> p_proxy) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_imageViewer(std::move(p_imageViewer)),
    m_reader(std::move(p_reader)),
    m_proxy(std::move(p_proxy)),
    m_isBrain3DMode(true)
{
    qDebug() << "Przed setupem UI";
    ui->setupUi(this);
    this->setWindowTitle(trUtf8("Dicom Reader"));

    m_renderWindow = ui->qvtkWidget->GetRenderWindow();
    m_imageViewer->setRenderWindow(m_renderWindow);
    m_imageViewer->changeOrientation(TPlane::BRAIN3D);

    ui->actionKosci->setToolTip("W danych CT pozwala na wyekstrahowanie części odpowiedzialnych za widok kości");
    ui->actionSkora->setToolTip("W danych CT pozwala na wyekstrahowanie części odpowiedzialnych za widok skóry");
    ui->Wiersze->setToolTip("Liczba wierszy w montażu");
    ui->Kolumna->setToolTip("Liczba kolumn w montażu");
    ui->PrzesuwakSlice->setToolTip("Pozycja pierwszego slice'a");
    ui->sagittalSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 2");
    ui->coronalSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 3");
    ui->axialSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 1");
}

void MainWindow::on_actionWczytaj_Dicom_triggered()
{
    qDebug() << "on_actionWczytaj_Dicom_triggered";
    QString l_filename = QFileDialog::getOpenFileName(this, "File with medical image", "/home",
                                                      tr("Medical Image Files (*.nii *.dcm *.mnc *.mhd);;"
                                                         "NIfTI(*.nii);;DICOM(*.dcm);;"
                                                         "MINC(*.mnc);;"
                                                         "MHD(*.mhd)"));
    if(l_filename.isEmpty())
    {
        return;
    }

    m_reader->readDicomImages(l_filename);
    m_imageViewer->setInputConnection(m_reader->GetOutputPort());
    m_proxy->SetInputConnection(m_reader->GetOutputPort());

    auto l_images = m_proxy->GetInput();
    int l_sliceDimension[3];
    l_images->GetDimensions(l_sliceDimension);

    ui->axialSlider->setMaximum(l_sliceDimension[2]);
    ui->coronalSlider->setMaximum(l_sliceDimension[1]);
    ui->sagittalSlider->setMaximum(l_sliceDimension[0]);
    ui->PrzesuwakSlice->setMaximum(std::max(l_sliceDimension[0], l_sliceDimension[2], std::greater<int>()));

    m_imageViewer->render();
}

void MainWindow::on_actionWidok_pierwszy_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::AXIAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->Wiersze->hide();
    ui->Kolumna->hide();
    ui->PrzesuwakSlice->hide();
    ui->axialSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 1");
}

void MainWindow::on_actionWidok_drugi_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::CORONAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->Wiersze->hide();
    ui->Kolumna->hide();
    ui->PrzesuwakSlice->hide();
    ui->axialSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 2");
}

void MainWindow::on_actionWidok_trzeci_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::SAGITAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->Wiersze->hide();
    ui->Kolumna->hide();
    ui->PrzesuwakSlice->hide();
    ui->axialSlider->setToolTip("Przesuwak kolejnych slajsów w widoku 3");
}

void MainWindow::on_actionBrain3D_triggered()
{
    m_isBrain3DMode = true;
    m_imageViewer->changeOrientation(TPlane::BRAIN3D);
    ui->axialSlider->show();
    ui->coronalSlider->show();
    ui->sagittalSlider->show();
    ui->Wiersze->hide();
    ui->Kolumna->hide();
    ui->PrzesuwakSlice->hide();
}

void MainWindow::on_actionMontaz_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->setMontageParameters(0, 5, 5, {ui->qvtkWidget->size().height(), ui->qvtkWidget->size().width()});
    m_imageViewer->changeOrientation(TPlane::MONTAGE);
    ui->axialSlider->hide();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->Wiersze->show();
    ui->Kolumna->show();
    ui->PrzesuwakSlice->show();
}

void MainWindow::on_actionPodstawowy_Brain3D_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::BASIC3D);
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->axialSlider->hide();
    ui->Wiersze->hide();
    ui->Kolumna->hide();
    ui->PrzesuwakSlice->hide();
}

void MainWindow::on_axialSlider_valueChanged(int p_slice)
{
    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::AXIAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_coronalSlider_valueChanged(int p_slice)
{
    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::CORONAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_sagittalSlider_valueChanged(int p_slice)
{

    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::SAGITAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_actionSkora_triggered()
{
    m_imageViewer->setMode(TMode::skin);
    m_imageViewer->render();
}

void MainWindow::on_actionKosci_triggered()
{
    m_imageViewer->setMode(TMode::bone);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_1_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::AXIAL);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_2_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::CORONAL);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_3_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::SAGITAL);
    m_imageViewer->render();
}

void MainWindow::on_Wiersze_valueChanged(int p_rows)
{
    m_imageViewer->setMontageParameters(ui->PrzesuwakSlice->value(),
                                        p_rows, ui->Kolumna->value(),
                                        {ui->qvtkWidget->size().height(), ui->qvtkWidget->size().width()});
    m_imageViewer->changeOrientation(TPlane::MONTAGE);
}

void MainWindow::on_Kolumna_valueChanged(int p_cols)
{
    m_imageViewer->setMontageParameters(ui->PrzesuwakSlice->value(),
                                        ui->Wiersze->value(), p_cols,
                                        {ui->qvtkWidget->size().height(), ui->qvtkWidget->size().width()});
    m_imageViewer->changeOrientation(TPlane::MONTAGE);
}

void MainWindow::on_PrzesuwakSlice_valueChanged(int p_slice)
{
    m_imageViewer->setMontageParameters(ui->PrzesuwakSlice->value(),
                                        ui->Wiersze->value(), ui->Kolumna->value(),
                                        {ui->qvtkWidget->size().height(), ui->qvtkWidget->size().width()});
    m_imageViewer->changeOrientation(TPlane::MONTAGE);
}

void MainWindow::on_actionO_Qt_triggered()
{
    QMessageBox::about(this, "O tym co używamy", "QT 5.9.1 + VTK 8.0 pod systemem windows");
}

void MainWindow::on_actionO_Autorze_triggered()
{
    QMessageBox::about(this, "Kontakt do autora aplikacji", "Michał Kluska - michaekluska@gmail.com \nAnaliza i przetwarzanie obrazów medycznych 2017/2018");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionZamknij_triggered()
{
    qApp->exit();
}

#pragma once

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

using vtkImage = vtkSmartPointer<vtkImageData>;

#pragma once

#include <QString>
#include "vtkAlgorithmOutput.h"

class IDicomImageReader
{
public:
    ~IDicomImageReader() = default;

    virtual void readDicomImages(QString p_name)  = 0;
    virtual vtkAlgorithmOutput* GetOutputPort()  = 0;
};

#pragma once

#include "idicomimageReader.h"

#include <string>
#include <vector>
#include <QStringList>

#include <armadillo>

#include <vtkSmartPointer.h>
#include <vtkImageReader2.h>
#include <vtkDICOMImageReader.h>
#include <vtkMetaImageReader.h>
#include <vtkImageShiftScale.h>

class DicomImageReader : public IDicomImageReader
{
public:
    vtkImage readDicomImages(QString p_name) const override;

private:
    vtkImage readDicomSlicesFromDirectory(QString p_directoryName) const;
    vtkImage readDicomSlicesFromFile(QString p_fileName) const;
    bool isDirectory(QString p_name) const;
    vtkImage getVtkImageDataFromDirectory(QString p_directory) const;
    vtkImage getVtkImageDataFromFile(QString p_fileName,
                                     vtkSmartPointer<vtkImageReader2> p_imageReader =
                                         vtkSmartPointer<vtkDICOMImageReader>::New(),
                                     vtkSmartPointer<vtkImageShiftScale> p_imageScaler =
                                         vtkSmartPointer<vtkImageShiftScale>::New()) const;
    void setSpecificImageReader(QString& p_filename) const;
    QStringList getFilesFromDirectory(QString p_directory) const;
    vtkImage generateVtkImageFromDirectory(QStringList& p_filenames) const;

    vtkSmartPointer<vtkImageReader2> m_reader;
};

#include "dicomimagerader.h"

#include <QtDebug>

#include <QFileDialog>
#include <QDirIterator>

#include <vtkDICOMImageReader.h>
#include <vtkMINCImageReader.h>
#include <vtkNIFTIImageReader.h>
#include <vtkImageAppend.h>
#include <vtkMetaImageReader.h>

#include <vtkImageData.h>

void DicomImageReader::readDicomImages(QString p_name)
{
    if(isDirectory(p_name))
    {
        m_data = readDicomSlicesFromDirectory(p_name);
        return;
    }
    m_data = readDicomSlicesFromFile(p_name);
}

vtkAlgorithmOutput* DicomImageReader::GetOutputPort()
{
    return m_data;
}

vtkAlgorithmOutput* DicomImageReader::readDicomSlicesFromDirectory(QString p_directoryName)
{
    auto l_filesList = getFilesFromDirectory(p_directoryName);
    return generateVtkImageFromDirectory(l_filesList);
}

vtkAlgorithmOutput* DicomImageReader::readDicomSlicesFromFile(QString p_fileName)
{
    auto l_reader = getSpecificImageReader(p_fileName);
    return getVtkDataDataFromFile(p_fileName, std::move(l_reader));
}

bool DicomImageReader::isDirectory(QString p_name)
{
    if(p_name.indexOf('.') == -1)
    {
        return true;
    }
    return false;
}

vtkSmartPointer<vtkImageReader2> DicomImageReader::getSpecificImageReader(QString& p_filename)
{
    int l_dotPosition = p_filename.lastIndexOf('.');
    QString l_fileExtension = p_filename.mid(l_dotPosition + 1);

    if(l_fileExtension == "dcm")
    {
        return vtkSmartPointer<vtkDICOMImageReader>::New();
    }

    if(l_fileExtension == "nii")
    {
        return vtkSmartPointer<vtkNIFTIImageReader>::New();
    }

    if(l_fileExtension == "mnc")
    {
        qDebug() << "Mnc reader";
        return vtkSmartPointer<vtkMINCImageReader>::New();
    }

    if(l_fileExtension == "mhd")
    {
        qDebug() << "Mhd reader";
        return vtkSmartPointer<vtkMetaImageReader>::New();
    }
    return nullptr;
}

vtkAlgorithmOutput* DicomImageReader::getVtkDataDataFromFile(QString p_fileName,
                                                             vtkSmartPointer<vtkImageReader2> p_reader)
{
    qDebug() << "SetFile" << p_fileName;
    p_reader->SetFileName(p_fileName.toStdString().c_str());
    qDebug() << "After setFileName";
    p_reader->Update();

    qDebug() << "After Update";
    m_scaler->SetInputConnection(p_reader->GetOutputPort());
    qDebug() << "After setInputConnection";
    m_scaler->SetOutputScalarTypeToShort();
    m_scaler->Update();

    return m_scaler->GetOutputPort();
}

vtkSmartPointer<vtkImageData> DicomImageReader::getVtkImageDataFromFile(QString p_fileName)
{
    m_reader->SetFileName(p_fileName.toStdString().c_str());
    m_reader->Update();

    m_scaler->SetInputConnection(m_reader->GetOutputPort());
    m_scaler->SetOutputScalarTypeToShort();
    m_scaler->Update();

    return vtkSmartPointer<vtkImageData>(m_scaler->GetOutput());
}

QStringList DicomImageReader::getFilesFromDirectory(QString p_directory)
{
    QDir l_currentDir(p_directory);
    l_currentDir.setNameFilters(QStringList() << "*.dcm");
    auto l_filesName = l_currentDir.entryList(QDir::Files, QDir::Name);

    for(auto & l_filename : l_filesName)
    {
        l_filename = p_directory + "/" + l_filename;
    }

    return l_filesName;
}

vtkAlgorithmOutput* DicomImageReader::generateVtkImageFromDirectory(QStringList& p_filenames)
{
    m_appender->RemoveAllInputs();
    m_appender->SetAppendAxis(2);

    for(const auto& l_file : p_filenames)
    {
         const auto& l_vtkImage = getVtkImageDataFromFile(l_file);
         l_vtkImage->SetOrigin(0, 0, 0);
         m_appender->AddInputData(std::move(l_vtkImage));
    }
    m_appender->Update();

    return m_appender->GetOutputPort();
}

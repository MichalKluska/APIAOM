#ifndef DICOMIMAGERADER_H
#define DICOMIMAGERADER_H

#include "idicomimagereader.h"
#include <vtkImageReader2.h>
#include <vtkImageShiftScale.h>
#include <vtkDICOMImageReader.h>
#include <QStringList>
#include <vtkImageAppend.h>
#include <vtkSmartPointer.h>

class DicomImageReader : public IDicomImageReader
{
public:
    void readDicomImages(QString p_name) override;
    vtkAlgorithmOutput* GetOutputPort() override;
private:
    vtkAlgorithmOutput* readDicomSlicesFromDirectory(QString p_directoryName) ;
    vtkAlgorithmOutput* readDicomSlicesFromFile(QString p_fileName) ;
    bool isDirectory(QString p_name) ;
    vtkAlgorithmOutput* getVtkImageDataFromDirectory(QString p_directory) ;
    vtkAlgorithmOutput* getVtkDataDataFromFile(QString p_fileName, vtkSmartPointer<vtkImageReader2> p_reader) ;
    vtkSmartPointer<vtkImageData> getVtkImageDataFromFile(QString p_fileName) ;
    vtkSmartPointer<vtkImageReader2> getSpecificImageReader(QString& p_filename);
    QStringList getFilesFromDirectory(QString p_directory) ;
    vtkAlgorithmOutput* generateVtkImageFromDirectory(QStringList& p_filenames) ;

    vtkSmartPointer<vtkImageReader2> m_reader;
    vtkSmartPointer<vtkImageShiftScale> m_scaler = vtkSmartPointer<vtkImageShiftScale>::New();
    vtkSmartPointer<vtkImageAppend> m_appender = vtkSmartPointer<vtkImageAppend>::New();

    vtkAlgorithmOutput* m_data;
};


#endif // DICOMIMAGERADER_H
